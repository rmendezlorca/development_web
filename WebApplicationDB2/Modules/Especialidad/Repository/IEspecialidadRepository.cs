﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplicationDB2.Modules.Utils;

namespace WebApplicationDB2.Modules.Especialidad.Repository
{
    interface IEspecialidadRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
    }
}
