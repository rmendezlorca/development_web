﻿using Spring.Aop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebApplicationDB2.AOP
{
    public class LoggingBeforeAdvice:IMethodBeforeAdvice
    {
      

        public void Before(MethodInfo method, object[] args, object target)
        {
            
            Dictionary<string, string> dict = new Dictionary<string, string>();
            ParameterInfo[] para = method.GetParameters();
            for (int i=0;i< method.GetParameters().Length; i++)
            {
                var value = args[i];
                var par=method.GetParameters()[i];
                dict.Add(par.Name, value.ToString());
                Type tp = target.GetType();
                PropertyInfo pi=tp.GetProperty("Map");
                PropertyInfo[] pinfo = tp.GetProperties();
                //Object ob=pi.GetValue(tp,null);
                //pi.SetValue(tp, ob, null);
                
                Object ob = pi.GetValue(target, null);
                pi.SetValue(target, dict,null);
                

            }
            
            Console.Out.WriteLine("Antes del metodo");
        }
    }
    
}