﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationDB2.Modules.Utils;

using System.Data.Entity;

namespace WebApplicationDB2.Modules.Especialista.Repository
{
    public class EspecialistaRepository : GenericRepository<WebApplicationDB2.Modules.Especialista.Domain.Entity.Especialista>, IEspecialistaRepository<WebApplicationDB2.Modules.Especialista.Domain.Entity.Especialista>
    {
        public EspecialistaRepository(DbContext context) : base(context) { }
    }
}