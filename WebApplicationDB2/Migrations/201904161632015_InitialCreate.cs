namespace WebApplicationDB2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.especialidad",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.especialista",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Nombres = c.String(),
                        Apellidos = c.String(),
                        Direccion = c.String(),
                        Telefono = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.examen",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Descripcion = c.String(),
                        Preparacion = c.String(),
                        Duracion = c.String(),
                        TiempoEntregaResultado = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.usuario",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        nombres = c.String(),
                        apellidos = c.String(),
                        email = c.String(),
                        fono = c.String(),
                        email_personal = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("public.usuario");
            DropTable("public.examen");
            DropTable("public.especialista");
            DropTable("public.especialidad");
        }
    }
}
