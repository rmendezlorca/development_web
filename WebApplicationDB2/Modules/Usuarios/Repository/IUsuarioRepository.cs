﻿using System.Collections.Generic;
using WebApplicationDB2.Modules.Utils;

namespace WebApplicationDB2.Modules.Usuarios.Repository
{
    public interface IUsuarioRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        TEntity FindByLogin(string login);
        List<TEntity> getAll();
        TEntity Get();
    }
}
