﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplicationDB2.Modules.Usuarios.Domain;

namespace WebApplicationDB2.Modules.Usuarios.Application
{
    interface IUsuarioApplication
    {
        int Persist(Usuario usr);
        Usuario FindByLogin(string login);
        Boolean ExitsLogin(string login);
    }
}
