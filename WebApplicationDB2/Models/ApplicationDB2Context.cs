﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApplicationDB2.Models
{
    public class ApplicationDB2Context : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        public ApplicationDB2Context() : base("name=ApplicationDB2Context")
        {
        }



        public System.Data.Entity.DbSet<WebApplicationDB2.Modules.Usuarios.Domain.Usuario> Usuarios { get; set; }
        public DbSet<Modules.Especialista.Domain.Entity.Especialista> Especialistas { get; set; }
        public DbSet<Modules.Examen.Domain.Entity.Examen> Examens { get; set; }
        public DbSet<Modules.Especialidad.Domain.Entity.Especialidad> Especialidads { get; set; }
    }
}
