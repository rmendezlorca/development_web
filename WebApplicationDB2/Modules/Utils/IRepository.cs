﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplicationDB2.Modules.Utils
{
    public interface IRepository<TEntity> where TEntity : class, new()
    {
        int New(TEntity entity);
        int Merge(TEntity entity);
        TEntity Add(TEntity entity);
        void Delete(TEntity entity);
    }
}
