﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplicationDB2.Modules.Usuarios.Domain;
using WebApplicationDB2.Modules.Utils;

namespace WebApplicationDB2.Modules.Usuarios.Repository
{
    public class UsuarioRepository : GenericRepository<Usuario>, IUsuarioRepository<Usuario>
    {
        public UsuarioRepository(DbContext context) : base(context) { }

        public Usuario Get()
        {
            return GetAll().First();
        }

        Usuario IUsuarioRepository<Usuario>.FindByLogin(string login)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>();
            dict.Add("Nombres", login.ToString());
            List<Usuario> list_usuario=FindByProperties(new Usuario(), dict,true);
            return list_usuario.First();
        }
        

        List<Usuario> IUsuarioRepository<Usuario>.getAll()
        {
            return GetAll().ToList();
        }
        /* Usuario IUsuarioRepository<Usuario>.FindByLogin(string login)
{
    Dictionary<string, string> dict = new Dictionary<string, string>();
    dict.Add("login", login.ToString());
    List<Usuario> list_usuario = FindByProperties(new Usuario(), dict, true);
    return list_usuario.First();
}*/
    }
}