﻿using Spring.Context;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplicationDB2.Models;
using WebApplicationDB2.Modules.Especialista.Domain.Entity;
using WebApplicationDB2.Modules.Especialista.Repository;

namespace WebApplicationDB2.Controllers
{
    public class EspecialistasController : Controller
    {
        private ApplicationDB2Context db = new ApplicationDB2Context();

        // GET: Especialistas
        public ActionResult Index()
        {
            return View(db.Especialistas.ToList());
        }

        // GET: Especialistas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialista especialista = db.Especialistas.Find(id);
            if (especialista == null)
            {
                return HttpNotFound();
            }
            return View(especialista);
        }

        // GET: Especialistas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Especialistas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombres,Apellidos,Direccion,Telefono,Email")] Especialista especialista)
        {
            EspecialistaRepository especialistaRepository = new EspecialistaRepository(db);
            if (ModelState.IsValid)
            {
                IApplicationContext applicationContext = ContextRegistry.GetContext();
                IEspecialistaRepository<Especialista> usr = applicationContext.GetObject("especialista_repository") as IEspecialistaRepository<Especialista>;
                usr.New(especialista);
                //especialistaRepository.New(especialista);
                //db.Especialistas.Add(especialista);
                //db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(especialista);
        }

        // GET: Especialistas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialista especialista = db.Especialistas.Find(id);
            if (especialista == null)
            {
                return HttpNotFound();
            }
            return View(especialista);
        }

        // POST: Especialistas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombres,Apellidos,Direccion,Telefono,Email")] Especialista especialista)
        {
            if (ModelState.IsValid)
            {
                db.Entry(especialista).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(especialista);
        }

        // GET: Especialistas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Especialista especialista = db.Especialistas.Find(id);
            if (especialista == null)
            {
                return HttpNotFound();
            }
            return View(especialista);
        }

        // POST: Especialistas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Especialista especialista = db.Especialistas.Find(id);
            db.Especialistas.Remove(especialista);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Especialista() {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
