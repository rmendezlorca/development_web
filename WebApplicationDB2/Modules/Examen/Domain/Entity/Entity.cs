﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplicationDB2.Modules.Examen.Domain.Entity
{
    [Table("public.examen")]
    public class Examen
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public string Preparacion { get; set; }
        public string Duracion { get; set; }
        public string TiempoEntregaResultado { get; set; }
    }
}