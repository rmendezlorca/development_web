﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplicationDB2.Modules.Especialidad.Domain.Entity
{
    [Table("public.especialidad")]
    public class Especialidad
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        
        public string Descripcion { get; set; }
        
    }
}