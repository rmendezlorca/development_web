﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplicationDB2.Modules.Usuarios.Domain
{
    [Table("public.usuario")]
    public class Usuario
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        //[Column("id")]
        public int Id { get; set; }
        [Column("nombres")]
        public string Nombres { get; set; }
        [Column("apellidos")]
        public string Apellidos { get; set; }
        [Column("email")]
        public string Email { get; set; }
        [Column("fono")]
        public string Telefono { get; set; }
        [Column("email_personal")]
        public string EmailPersonal { get; set; }
    }
}