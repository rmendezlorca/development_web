﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebApplicationDB2.Modules.Utils
{
    public abstract class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        DbContext _context { get; set; }
        public GenericRepository(DbContext context) {
            _context = context;
        }

        public TEntity Add(TEntity entity)
        {
            return (_context.Set<TEntity>()).Add(entity);
        }

        public void Delete(TEntity entity)
        {
            (_context.Set<TEntity>()).Attach(entity);
            (_context.Set<TEntity>()).Remove(entity);
            
        }

        public int Merge(TEntity entity)
        {
            (_context.Set<TEntity>()).Attach(entity);
            return _context.SaveChanges();
        }

        public int New(TEntity entity)
        {
            (_context.Set<TEntity>()).Add(entity);
            int id= _context.SaveChanges();
            return id;
            
            
        }
        public TEntity FindById(int id) {
            return (_context.Set<TEntity>()).Find(id);
        }
        public IEnumerable<TEntity> GetAll()
        {
            return (_context.Set<TEntity>()).AsEnumerable<TEntity>();
        }
        private PropertyInfo GetProperty(TEntity entity, string propertyName)
        {
            Type objectType = entity.GetType();
            PropertyInfo property = objectType.GetProperty(propertyName);
            return property;
        }
        private string GetTypeByProperty(TEntity entity, string propertyName)
        {
            PropertyInfo property = GetProperty(entity, propertyName);
            Type tp = property.PropertyType;
            TypeInfo ti = tp.GetTypeInfo();
            Type tipo = ti.GetType();
            string name = ti.FullName;
            return name;
        }
        private bool ExistsProperty(TEntity entity, string propertyName)
        {
            PropertyInfo property = GetProperty(entity, propertyName);
            return property != null && property.Name.Equals(propertyName);
        }
        private Object EvalPropertiesStatement(Object name, Object valor, bool parseString = true)
        {
            Object value = null;
            if (name.ToString().Contains("Int32"))
            {

                value = int.Parse(valor.ToString());
            }
            else
            {
                if (name.ToString().Contains("String"))
                {
                    if (parseString)
                    {
                        value = "'" + valor + "'";
                    }
                    else
                    {
                        value = valor;
                    }
                }
                else
                {
                    if (valor.Equals("True"))
                    {
                        value = "1";
                    }
                    else
                    {
                        if (valor.Equals("False"))
                        {
                            value = "0";
                        }
                        else
                        {
                            if (name.ToString().Contains("DateTime"))
                            {
                                value = "'" + valor.ToString().Replace("-", "") + "'";
                            }
                        }
                    }
                }
            }
            return value;
        }
        public string GetTableNameByEntity(TEntity entity)
        {
            string classname = entity.ToString();

            string[] nameclazz = classname.Split('.');
            string tablename = nameclazz[1];
            return tablename;
        }
        public List<TEntity> FindByProperties(TEntity entity, Dictionary<string, string> properties, bool andOperator)
        {
            string andStatement = "Where ";
            string operador = andOperator ? " and " : " or ";
            foreach (KeyValuePair<string, string> props in properties)
            {
                string key = props.Key;
                if (!ExistsProperty(entity, key))
                {
                    throw new Exception("No existe la propiedad requerida para consultar");
                }
                Object name = GetTypeByProperty(entity, key);
                Object value = null;

                value = EvalPropertiesStatement(name, props.Value);

                andStatement += " " + props.Key + " = " + value + "" + operador;
            }
            int largo = andStatement.Length - 4;
            string andStatementfinal = andStatement.Remove(andStatement.Length - 4);
            string tablename = GetTableNameByEntity(entity);
            string query = "Select * from " + tablename + " " + andStatementfinal;
            return _context.Set<TEntity>().SqlQuery(query).ToList();
        }
        public List<TEntity> FindByProperties(TEntity entity, string propertyName, Object value)
        {
            if (!ExistsProperty(entity, propertyName))
            {
                throw new Exception("No existe la propiedad requerida para consultar");
            }

            string name = GetTypeByProperty(entity, propertyName);
            string andStatement = " Where ";
            value = EvalPropertiesStatement(name, value);
            andStatement += " " + propertyName + " = " + value + " and ";
            string tablename = GetTableNameByEntity(entity);
            string andStatementfinal = andStatement.Remove(andStatement.Length - 4);
            string query = "Select * from " + tablename + "  " + andStatementfinal;
            return _context.Set<TEntity>().SqlQuery(query).ToList();
        }
    }

}