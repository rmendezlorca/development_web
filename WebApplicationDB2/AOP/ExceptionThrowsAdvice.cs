﻿using Spring.Aop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationDB2.AOP
{
    public class ExceptionThrowsAdvice : IThrowsAdvice
    {
        public void AfterThrowing(ApplicationException ex)
        {
            Console.Out.WriteLine("Una vez lanzada una excepcion.");
        }
    }
}