﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebApplicationDB2.Modules.Utils;

namespace WebApplicationDB2.Modules.Especialidad.Repository
{
    public class EspecialidadRepository : GenericRepository<WebApplicationDB2.Modules.Especialidad.Domain.Entity.Especialidad>, IEspecialidadRepository<WebApplicationDB2.Modules.Especialidad.Domain.Entity.Especialidad>
    {
        public EspecialidadRepository(DbContext context) : base(context)
        {
        }
    }
}