﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplicationDB2.Modules.Usuarios.Domain;
using WebApplicationDB2.Modules.Usuarios.Repository;

namespace WebApplicationDB2.Modules.Usuarios.Application
{
    public class UsuarioApplication : IUsuarioApplication
    {
        IUsuarioRepository<Usuario> _ir;
        public UsuarioApplication(IUsuarioRepository<Usuario> iur)
        {
            _ir = iur;
        }
        public int Persist(Usuario usr)
        {
            return _ir.New(usr);
        }
        public Usuario FindByLogin(string login)
        {
            return _ir.FindByLogin(login);
        
            
        }
        public Boolean ExitsLogin(string login)
        {
            Usuario usr = FindByLogin(login);
            if (usr!= null)
            {
                return true;
            }
            return false;
        }

    }
}