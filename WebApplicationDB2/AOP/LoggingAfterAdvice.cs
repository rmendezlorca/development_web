﻿using Spring.Aop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebApplicationDB2.AOP
{
    public class LoggingAfterAdvice : IAfterReturningAdvice
    {
        void IAfterReturningAdvice.AfterReturning(object returnValue, MethodInfo method, object[] args, object target)
        {
            Console.Write("Despues del metodo");
        }
    }
}