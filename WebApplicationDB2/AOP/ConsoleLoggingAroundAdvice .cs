﻿using AopAlliance.Intercept;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationDB2.AOP
{
    class ConsoleLoggingAroundAdvice : IMethodInterceptor
    {
        object IMethodInterceptor.Invoke(IMethodInvocation invocation)
        {
            Console.Out.WriteLine("Advice executing; calling the advised method...");
            object returnValue = invocation.Proceed();
            Console.Out.WriteLine("Advice executed; advised method returned " + returnValue);
            return returnValue;
        }
    }
}